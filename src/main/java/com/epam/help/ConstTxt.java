package com.epam.help;

public class ConstTxt {

    final static String CHOOSE_LANGUAGE="Please chose a language \n" + "\n1) English" + "\n2) Українська \n";
    final static String INVALID_VARIABLE ="\nInvalid variable, please enter from 1 to 2\n";
    final static String SET_VALUE = "Please set your value: ";
    final static String REFLECTION_PATH ="com.epam";

    public ConstTxt() {
    }

    public static String getChooseLanguage() {
        return CHOOSE_LANGUAGE;
    }

    public static String getInvalidVariable() {
        return INVALID_VARIABLE;
    }

    public static String getSetValue() {
        return SET_VALUE;
    }

    public static String getReflectionPath() {
        return REFLECTION_PATH;
    }
}
