package com.epam.annotations;

public @interface PrintThoseFieldsAnnotation {

    String value() default "We don't have this information";
}
