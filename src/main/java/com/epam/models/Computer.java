package com.epam.models;

import com.epam.annotations.PrintThoseFieldsAnnotation;
import java.util.Objects;

@PrintThoseFieldsAnnotation("this is computer class")
public class Computer {

    private String computeBrand;
    private String computerModel;
    private int computerCores;

    public Computer(String computeBrand, String computerModel, int computerCores) {
        this.computeBrand = computeBrand;
        this.computerModel = computerModel;
        this.computerCores = computerCores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Computer computer = (Computer) o;
        return computerCores == computer.computerCores &&
                Objects.equals(computeBrand, computer.computeBrand) &&
                Objects.equals(computerModel, computer.computerModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(computeBrand, computerModel, computerCores);
    }

    @Override
    public String toString() {
        return "Computer{" +
                "computeBrand='" + computeBrand + '\'' +
                ", computerModel='" + computerModel + '\'' +
                ", computerCores=" + computerCores +
                '}';
    }

}
