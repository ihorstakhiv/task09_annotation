package com.epam.models;

import com.epam.annotations.PrintThoseFieldsAnnotation;
import java.util.Objects;

@PrintThoseFieldsAnnotation
public class Laptop {

    private int laptopSale;
    private int laptopCores;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Laptop laptop = (Laptop) o;
        return laptopSale == laptop.laptopSale &&
                laptopCores == laptop.laptopCores;
    }

    @Override
    public int hashCode() {
        return Objects.hash(laptopSale, laptopCores);
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "laptopSale=" + laptopSale +
                ", laptopCores=" + laptopCores +
                '}';
    }
}
