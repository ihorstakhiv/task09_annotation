package com.epam.view;

import com.epam.help.ConstTxt;
import com.epam.controllers.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class ConsoleView implements View {

    private Locale locale;
    private ResourceBundle resourceBundle;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() {
        input = new Scanner(System.in);
        controller = new Controller();
        logger.info(ConstTxt.getChooseLanguage());
        try {
            int choose = input.nextInt();
            if (choose == 1)
                englishMenu();
            if (choose == 2)
                ukraineMenu();
            if (choose > 2)
                throw new NullPointerException();
        } catch (Exception exception) {
            logger.info(ConstTxt.getInvalidVariable());
            new ConsoleView();
        }
    }

    private void putResourceBundle() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("6", resourceBundle.getString("6"));
    }

    private void putMethods() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printAllClassesAndHisFieldsWithThisAnnotation);
        methodsMenu.put("2", this::printAnnotationValueIntoConsole);
        methodsMenu.put("3", this::printInvokeMethodThreeMethodWithDifferentParametersAnd);
        methodsMenu.put("4", this::printFieldNotKnowingItsType);
        methodsMenu.put("5", this::printAllInformationAboutYourClass);
        methodsMenu.put("6", this::languageMenu);
    }

    private void printAllClassesAndHisFieldsWithThisAnnotation() {
        logger.info(controller.allClassesAndHisFieldsWithThisAnnotation());
    }

    private void printAnnotationValueIntoConsole() {
        logger.info(controller.allClassesAndAnnotationValues());
    }


    private void printInvokeMethodThreeMethodWithDifferentParametersAnd() {
        logger.info(controller.typeOfMethod());
    }

    private void printFieldNotKnowingItsType() {
        logger.info(ConstTxt.getSetValue());
        try {
            logger.info(controller.setValueIntoFieldNotKnowingItsType(input.nextInt()));
        } catch (Exception ex) {
            logger.error(ex.getStackTrace());
            logger.info(controller.setValueIntoFieldNotKnowingItsType(input.nextLine()));
        }
    }

    private void printAllInformationAboutYourClass() {
        controller.allInformationAboutClass();
    }


    private void englishMenu() {
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        putResourceBundle();
        putMethods();
        show();
    }

    private void ukraineMenu() {
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        putResourceBundle();
        putMethods();
        show();
    }

    private void languageMenu() {
        new ConsoleView();
    }

    private void outputMenu() {
        logger.info("\n*********************************************       MENU       *********************************************   \n");
        for (String str : menu.values()) {
            logger.info("\n" + str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("\nPlease, select menu point:\n");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}