package com.epam.controllers;

import com.epam.help.ConstTxt;
import com.epam.annotations.MethodTypeAnnotation;
import com.epam.annotations.PrintThoseFieldsAnnotation;
import org.reflections.Reflections;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Controller<T> {

    private StringBuilder report;
    private Reflections reflections;

    public Controller() {
        report = new StringBuilder();
        reflections = new Reflections(ConstTxt.getReflectionPath());
    }

    public StringBuilder allClassesAndHisFieldsWithThisAnnotation() {
        for (Class<?> cl : reflections.getTypesAnnotatedWith(PrintThoseFieldsAnnotation.class)) {
            report.append("\nClass: " + cl.getSimpleName());
            report.append("\nFields: ");
            for (Field field : cl.getDeclaredFields()) {
                report.append(field.getName() + " ");
            }
            report.append("; \n");
        }
        return report;
    }

    public StringBuilder allClassesAndAnnotationValues() {
        report = new StringBuilder();
        for (Class<?> cl : reflections.getTypesAnnotatedWith(PrintThoseFieldsAnnotation.class)) {
            report.append(cl.getSimpleName());
        }
        return report;
    }

    public StringBuilder typeOfMethod() {
        report = new StringBuilder();
        for (Method method : reflections.getMethodsAnnotatedWith(MethodTypeAnnotation.class)) {
            report.append(method.getName()).append(": ").append(method.getReturnType().getSimpleName()).append(", \n");
        }
        return report;
    }

    public StringBuilder setValueIntoFieldNotKnowingItsType(T type) {
        report = new StringBuilder();
        return report.append(type.getClass().getName());
    }

    public StringBuilder allInformationAboutClass() {
        report = new StringBuilder();
        for (Class<?> classWithAnnotation : reflections.getTypesAnnotatedWith(PrintThoseFieldsAnnotation.class)) {
            report.append(classWithAnnotation.getSimpleName());
            for (Field field : reflections.getClass().getDeclaredFields()) {
                report.append(field.getName())
                        .append(field.getDeclaredAnnotations())
                        .append(field.getModifiers())
                        .append(field.getAnnotations());
            }
        }
        return report;
    }

    @MethodTypeAnnotation
    public int testMethodInt() {
        int i = 5 + 3;
        return i;
    }

    @MethodTypeAnnotation
    public String testMethodString() {
        String s = "String";
        return s;
    }

    @MethodTypeAnnotation
    public Double testMethodDouble() {
        double d = 5.2 + 3.4;
        return d;
    }

    @Override
    public String toString() {
        return "Controller{" +
                "reflections=" + reflections +
                '}';
    }

    public Controller(Reflections reflections) {
        this.reflections = reflections;
    }
}